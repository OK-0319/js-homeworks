'use strict';

let fieldset = document.createElement('fieldset');
document.body.append(fieldset);
fieldset.className = "wrapper";
fieldset.innerHTML= "<label id=price-label for=price-input>Price, $</label><input id=price-input type=number placeholder=0><span id=price-error></span>";

const priceInput = document.querySelector('#price-input');

priceInput.onfocus = (event) => {
    event.target.style.cssText = "border: 2px green solid;";
};

priceInput.onblur = (event) => {
    event.target.style.cssText = "";

    if (event.target.value < 0) {
        event.target.style.cssText = "border: 2px red solid;";
        const correctPrice = document.getElementById('price-error');
        correctPrice.style.display = "block";
        correctPrice.style.color = "firebrick";
        correctPrice.style.fontSize = "20px";
        correctPrice.innerText = 'Please enter correct price !';

        event.target.after(correctPrice);
    }
    else {
        if (event.target.value > 0) {
            event.target.style.cssText = "color: green;";
            document.getElementById('price-error').innerText = ``;
            const currentPriceWrapper = document.createElement('div');
            currentPriceWrapper.style.display = "inline";
            currentPriceWrapper.style.position = "absolute";
            currentPriceWrapper.style.margin = "6% 15%";
            currentPriceWrapper.style.color = "green";

            let currentPrice = document.createTextNode(`Текущая цена: ${event.target.value}`);

            const deleteBtn = document.createElement('button');
            deleteBtn.innerText = "x";
            deleteBtn.style.borderRadius = "45%";
            deleteBtn.style.color = "firebrick";
            deleteBtn.style.fontWeight = "750";

            currentPriceWrapper.appendChild(currentPrice);
            currentPriceWrapper.appendChild(deleteBtn);

            currentPriceWrapper.setAttribute('onclick', `removeDiv(this);`);

            fieldset.before(currentPriceWrapper);

            console.log(currentPrice);
        }
    }
};

let removeDiv = function(div) {
    fieldset.parentNode.removeChild(div);
};