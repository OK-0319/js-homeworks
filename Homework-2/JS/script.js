"use strict";

const divider = 5;

let maxLimit = parseInt(prompt("Please, enter the maximum number from 1 to 100", "99"));



while (maxLimit == "" || maxLimit == null || isNaN(maxLimit) || maxLimit >=100 || maxLimit <=0) {

    maxLimit = parseInt(prompt("Please, enter the maximum number from 1 to 100 AS NUMBER", `${maxLimit}`));

}

let minLimit = 0;

if (maxLimit < divider) {

    alert("Sorry, no numbers");

}

else {

    while (minLimit <= maxLimit) {

        minLimit++;

        if (minLimit % divider !== 0)

            continue;

        console.log(minLimit);
    }
}



let part2 = confirm("Проверить не обязательное задание продвинутой сложности ? ");
let min = 0;
let max = 0;

console.clear()
//////////////////////////////////////// ADVANCED  TASK
if (part2) {

    min = parseInt(prompt("Please, input the minimum number > 1", "2")),
    max = parseInt(prompt("Please, input the maximum number < 100", "99"));

    while (max, min == "" || min, max == null || isNaN(max, min) || min > max || min <= 1 || max > 100) {

        min = parseInt(prompt("Please, enter the MINIMUM number > 1, as NUMBER", `${min}`)),
        max = parseInt(prompt("Please, enter the MAXIMUM number < 100, as NUMBER", `${max}`));
    };

      while (min <= max) {
          min++;
          if (min % 2 ==0 || min % 3 ==0 || (min - 1) % min ==0 || min % 5 ==0 || min % 7 ==0 || min % 9 ==0)
              continue;
              console.log (min);
         }
} else {
                  alert("You decided not to proceed with advanced level task")
}