"use strict";

let iniNum = parseInt(prompt("Please, input the initial number >1 for factorial calculation", "20"));
console.log(iniNum);

while (iniNum == NaN || iniNum == undefined || iniNum == null || iniNum =="" || iniNum <=1) {

    iniNum = parseInt(prompt("Please, input the initial number > 1 for factorial calculation as NUMBER", `${iniNum}`));

}

function factorial(iniNum) {
    if(iniNum==0) {
        return 1;
    }
    return iniNum * factorial(iniNum-1);
}

console.log(factorial(iniNum));