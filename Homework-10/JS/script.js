'use strict';

const firstInput = document.getElementById('first-input');
const secondInput = document.getElementById('second-input');

let input1 = document.getElementById('first-password');
let input2 = document.getElementById('second-password');

const errorMessage = document.getElementById('error-message');
const submitBtn = document.getElementById("btn");

firstInput.addEventListener('click', function () {

    if (input1.type === 'password') {
        firstInput.classList.replace('fa-eye-slash', 'fa-eye');
        input1.type = 'text';
    }
    else
    {
        input1.type = 'password';
        firstInput.classList.replace('fa-eye', 'fa-eye-slash');
    }
});

secondInput.addEventListener('click', function () {

    if (input2.type === 'text') {
        secondInput.classList.replace('fa-eye', 'fa-eye-slash');
        input2.type = 'password';
    }
    else
    {
        input2.type = 'text';
        secondInput.classList.replace('fa-eye-slash', 'fa-eye');
    }
});

submitBtn.addEventListener('click', function (event) {

    event.preventDefault();

    if (input1.value === input2.value) {

        errorMessage.innerText = "";
        alert('You are welcome !');
    }
    else
    {
        errorMessage.innerText = 'Нужно ввести одинаковые значения !';
    }
});