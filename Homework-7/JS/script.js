'use strict';

const inputArray = ['hello', 'world', 'Kiev',

    'Kharkiv', 'Odessa', 'Lviv',['1', '2', '3', 'sea', 'user', 23]];

function getIncomingArray (inputArray) {
    console.log(inputArray);

}

getIncomingArray(inputArray);

function arrayToList (inputArray) {

    let listWrapper = document.getElementById("inputList");
    let listStructure = document.createElement('ol');
    listWrapper.appendChild(listStructure);

    let listItems = inputArray.map(function(elem) {

        if (Array.isArray(elem)) {
            let subList = '<ul>';

            elem.forEach(function(listItem) {
                subList += `<li>${listItem}</li>`;
            });
            subList += '</ul>';
            return subList;
        }
          else {
            return `<li>${elem}</li>`;
        }

    });

    console.log(listItems);

    listItems.forEach(function(listItem) {
        listStructure.innerHTML += listItem;
    });
};

arrayToList(inputArray);

let timeOut = 10;
let timerId = setInterval(function() {
    document.getElementById("timer").innerText = "We will clean the page in" + "  " + `${timeOut}` + "  " + "seconds";
    if (timeOut === 0) {
        document.getElementById('inputList').classList.add("clean-page");
        document.getElementById('timer').classList.add("clean-page");
        clearInterval(timerId);
    }
    timeOut--;
}, 1000);