'use strict';

window.onload = function () {

    let buttons = document.querySelectorAll('.btn');

    for (let i=0; i < buttons.length; i++) {
        buttons[i].id = `${buttons[i].innerText}`;
    }

    for (let i=0; i < buttons.length; i++) {
        buttons[i].id = (`${buttons[i].id}`.toLowerCase());
    }

    document.addEventListener('keydown', (event) => {

        let eventKey = (event.key.toLowerCase());
        
        for (let i=0; i < buttons.length; i++) {
            buttons[i].classList.remove('blue');

            if (buttons[i].id === eventKey.toLowerCase()) {
                buttons[i].classList.add('blue');
            }
        }
    });
};