"use strict";

const student = {

    firstName: undefined,
    lastName: undefined
};

function requestStudentIdentity(student) {

    let firstName = prompt("Please, input your first name", "Ivan");
    let lastName = prompt("Please, input your last name", "Kravchenko");

    student.firstName = firstName;
    student.lastName = lastName;

}

function createTabel() {

    let flag = true;
    let tabel = [];

    do {
        let subjectName = prompt("Please, input your subject", "Math");
        if (subjectName == null) {
            flag = false;
        }
        else {
            let subjectMark = +prompt("Please, input your mark (from 2 to 12) ", "4");
            tabel.push({

                subject: subjectName,
                mark: subjectMark
            });
        }
    } while (flag);
    return tabel;
}

function checkTabel (student) {
    let badMarkCounter = 0;

    student.tabel.forEach(subj => {

        if (subj.mark < 4)
            badMarkCounter++;
    });

    if (badMarkCounter > 0) {
        alert (" Student HAS NOT to be moved to the next term !");

    } else {
        alert (" Student is to be moved the next term !");
    }
    return badMarkCounter;
}

function getAverage (student) {
    let sumOfMarks = 0;
    let marksCounter = student.tabel.length;

    student.tabel.forEach(subj => {

        sumOfMarks += subj.mark;
    });

    let averageMark = sumOfMarks / marksCounter;

    if ( averageMark > 7) {

        alert (" Student has been awarded with the scholarship !");

    } else {
        alert (" NO scholarship !");
    }
    return averageMark;
}

requestStudentIdentity(student);

student.tabel = createTabel();

console.log (student);

let counter = checkTabel (student);
console.log("Qauntity of bad marks = " + `${counter}`);


let avrgMark = getAverage (student);
console.log ("The average mark is = " + `${avrgMark}`);