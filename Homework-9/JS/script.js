'use strict';


const tabsTitlesContainer = document.querySelector('.tabs');

tabsTitlesContainer.addEventListener('click', onContainerClick);

function onContainerClick(event) {
    const activeTabsTitle = event.target;
    const tabsTitlesParent = activeTabsTitle.parentElement;
    const tabsContentParent = document.querySelector('.tabs-content>li').parentElement;

    for (let i = 0; i < tabsContentParent.children.length; i++) {

        tabsContentParent.children[i].classList.add('hidden');
    }
    for(let i = 0; i < tabsTitlesParent.children.length; i++) {

        if (tabsTitlesParent.children[i].classList.contains('active')) {
            tabsTitlesParent.children[i].classList.remove('active');
        }
        activeTabsTitle.classList.add('active');

        let indexOfActiveTab;
        for (let i = 0; i <tabsTitlesParent.children.length; i++) {
            tabsContentParent.children[i].classList.add('hidden');

            if (tabsTitlesParent.children[i].classList.contains('active')) {
                indexOfActiveTab = i;
                tabsContentParent.children[i].classList.add('hidden');
                if (i === indexOfActiveTab) {
                    tabsContentParent.children[i].classList.remove('hidden');
                    break;
                }
            }
        }
    }
}