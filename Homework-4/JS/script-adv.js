"use strict"


function CreateNewUser() {

    let firstName = prompt("Please enter your first name", "Ivan");
    let lastName = prompt("Please enter your last name", "Kravchenko");

    this._firstName = firstName;
    this._lastName = lastName;

    Object.defineProperty(this, "firstName", {

        get: function () {
            return this._firstName;
        },

        writeable: false,
        configurable: false

    });

    Object.defineProperty(this, "lastName", {

        get: function () {
            return this._lastName;
        },

        writeable: false,
        configurable: false

    });

    this.getLogin = function () {
        return this.firstName.substring(0, 1).toLowerCase() + this.lastName.toLowerCase();
    };

    this.setFirstName = function(firstName) {
        this._firstName = firstName;
    };

    this.setLastName = function(lastName) {
        this._lastName = lastName;
    };
};

const newUser = new CreateNewUser();

console.log(newUser.getLogin());
console.log(newUser);