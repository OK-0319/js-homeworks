"use strict";

function filterBy(arr, dataType) {

    let positiveArr = arr.filter(function(elem) {
        return typeof (elem) !== dataType;
    });

    console.log(positiveArr);

    return positiveArr;
}

filterBy(['hello', 'world', 23, '23', null, "Яблоко", "Апельсин", "Груша"], 'string');

filterBy(['hello', 'world', 23, '23', null, +"Яблоко", "Апельсин", "Груша"], 'number');

filterBy(['hello', 'world', 23, '23', null, ["Яблоко", "Апельсин", "Груша"]], 'object');